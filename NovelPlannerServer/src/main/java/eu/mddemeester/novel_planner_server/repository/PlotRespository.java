/*
package eu.mddemeester.novel_planner_server.repository;

import eu.mddemeester.novel_planner_server.domain.Chapter;
import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.domain.Plot;
import eu.mddemeester.novel_planner_server.dto.ChapterDto;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import eu.mddemeester.novel_planner_server.dto.PlotDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlotRespository extends JpaRepository<Plot,Long> { //needs to stay plot because it queries the db

    public PlotDto getPlotById(Long id);

    public List<PlotDto> getPlotsByName(String name);
    public List<PlotDto> getPlotsByDescriptionContaining(String description);
    public List<PlotDto> getPlotsByCharacterListContaining(CharacterDto character);
    public List<PlotDto> getPlotsByChapterListContaining(ChapterDto chapter);

}
*/
