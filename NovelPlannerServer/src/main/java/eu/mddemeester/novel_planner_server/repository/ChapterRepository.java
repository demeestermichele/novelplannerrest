/*
package eu.mddemeester.novel_planner_server.repository;

import eu.mddemeester.novel_planner_server.domain.Chapter;
import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.domain.Plot;
import eu.mddemeester.novel_planner_server.dto.ChapterDto;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import eu.mddemeester.novel_planner_server.dto.PlotDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Qualifier("chapter")
@Repository
public interface ChapterRepository extends JpaRepository<Chapter, Long> {

//    public Chapter getChapters(Long id); //is this necessary when JPA already has a findAll function?
    public ChapterDto getChapterByNumber(Long number);
    public List<ChapterDto> findChaptersByNameContaining(String name);
    public List<ChapterDto> findChaptersByCharacterListContaining(CharacterDto character);
    public List<ChapterDto> findChaptersByPlotListContaining(PlotDto plot);
    public boolean existsById(Long id);

}
*/
