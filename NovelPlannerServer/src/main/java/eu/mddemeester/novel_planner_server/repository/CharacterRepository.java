package eu.mddemeester.novel_planner_server.repository;

import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author Michele de Meester
 */

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
    public CharacterDto getCharacterById(Long id);

}
