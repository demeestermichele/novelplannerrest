/*
package eu.mddemeester.novel_planner_server.service;

import eu.mddemeester.novel_planner_server.domain.Chapter;
import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.domain.Plot;
import eu.mddemeester.novel_planner_server.dto.ChapterDto;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import eu.mddemeester.novel_planner_server.dto.PlotDto;

import java.util.List;

public interface ChapterService {
    public boolean existChapter(Long id);

    //CREATE
    public ChapterDto addChapter(ChapterDto chapter);

    //READ
    public List<ChapterDto> getChapters();
    public ChapterDto getChapterId(Long id);
    public ChapterDto getChapterNumber(Long number);
    public List<ChapterDto> getChapterName(String name);
    public List<ChapterDto> getChapterByPlot(PlotDto plot);
    public List<ChapterDto> getChapterByCharacters(CharacterDto character);

    //UPDATE
    public  ChapterDto updateChapter(ChapterDto chapter);

    //DELETE
    public void deleteChapter(ChapterDto chapter); //is this chapter or boolean
}
*/
