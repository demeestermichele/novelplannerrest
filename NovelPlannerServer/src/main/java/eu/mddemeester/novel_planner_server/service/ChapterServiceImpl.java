/*
package eu.mddemeester.novel_planner_server.service;

import eu.mddemeester.novel_planner_server.domain.Chapter;
import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.domain.Plot;
import eu.mddemeester.novel_planner_server.dto.ChapterDto;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import eu.mddemeester.novel_planner_server.dto.PlotDto;
import eu.mddemeester.novel_planner_server.repository.ChapterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChapterServiceImpl implements ChapterService {

//    @Autowired
    private ChapterRepository chapterRepository;

    @Autowired
    public ChapterServiceImpl(ChapterRepository chapterRepository){
        this.chapterRepository = chapterRepository;
    }

    @Override
    public boolean existChapter(Long id) {
        return chapterRepository.existsById(id);
    }

    //CREATE
    @Override
    public ChapterDto addChapter(ChapterDto chapter) {
        if (chapter.getId() == null) return null;

        try {
            chapter = chapterRepository.save(chapter);
            return chapter;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    //READ
    @Override
    public List<ChapterDto> getChapters() {
        return chapterRepository.findAll();
    }

    @Override
    public ChapterDto getChapterId(Long id) {
        return chapterRepository.findById(id).orElse(null);
    }

    @Override
    public ChapterDto getChapterNumber(Long number) {
        return chapterRepository.getChapterByNumber(number);
    }

    @Override
    public List<ChapterDto> getChapterName(String name) {
        return chapterRepository.findChaptersByNameContaining(name);
    }

    @Override
    public List<ChapterDto> getChapterByPlot(PlotDto plot) {
        return chapterRepository.findChaptersByPlotListContaining(plot);
    }

    @Override
    public List<ChapterDto> getChapterByCharacters(CharacterDto character) {
        return chapterRepository.findChaptersByCharacterListContaining(character);
    }

    //UPDATE
    @Override
    public ChapterDto updateChapter(ChapterDto chapter) {
        if (chapter.getId() == null) return null;

        try {
            chapter = chapterRepository.save(chapter);
            return chapter;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //DELETE
    @Override
    public void deleteChapter(ChapterDto chapter) {
        chapterRepository.delete(chapter);
    }

}
*/
