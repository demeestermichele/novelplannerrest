/*
package eu.mddemeester.novel_planner_server.service;

import eu.mddemeester.novel_planner_server.dto.ChapterDto;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import eu.mddemeester.novel_planner_server.dto.PlotDto;
import eu.mddemeester.novel_planner_server.repository.PlotRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlotServiceImpl implements PlotService {

    @Autowired
    private PlotRespository plotRespository;

    @Autowired
    public PlotServiceImpl(PlotRespository plotRespository) {
        this.plotRespository = plotRespository;
    }

    @Override
    public List<PlotDto> getPlots() {
        return plotRespository.findAll();
    }

    @Override
    public boolean existPlot(Long id) {
        return plotRespository.existsById(id);
    }

    @Override
    public PlotDto addPlot(PlotDto plot) {
        if (plot.getId() == 0) return null;
        try {
            plot = plotRespository.save(plot);
            return plot;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public PlotDto getPlotById(Long id) {
        return plotRespository.getPlotById(id);
    }


    @Override
    public List<PlotDto> getsPlotsByName(String name) {
        return plotRespository.getPlotsByName(name);
    }

    @Override
    public List<PlotDto> getPlotsByCharacter(CharacterDto character) {
        return plotRespository.getPlotsByCharacterListContaining(character);
    }


    @Override
    public List<PlotDto> getPlotByChapter(ChapterDto chapter) {
        return plotRespository.getPlotsByChapterListContaining(chapter);
    }

*/
/*    @Override
    public PlotDto updatePlot(PlotDto plot) {
        if (plot.getId() != null) return null;
        try {
            plot =plotRespository.save(plot);
            return plot;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }*//*

*/
/*
    @Override
    public void deletePlot(PlotDto plot) {
        plotRespository.delete(plot);
    }*//*

}
*/
