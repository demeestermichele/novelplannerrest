/*
package eu.mddemeester.novel_planner_server.service;

import eu.mddemeester.novel_planner_server.domain.Chapter;
import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.domain.Plot;
import eu.mddemeester.novel_planner_server.dto.ChapterDto;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import eu.mddemeester.novel_planner_server.dto.PlotDto;

import java.util.List;

public interface PlotService {

    public boolean existPlot(Long id);

    //CREATE
    public PlotDto addPlot(PlotDto plot);

    //READ
    public PlotDto getPlotById(Long id);

    public List<PlotDto> getPlots();

    public List<PlotDto> getsPlotsByName(String name);

    public List<PlotDto> getPlotsByCharacter(CharacterDto character);

    public List<PlotDto> getPlotByChapter(ChapterDto chapter);

    //UPDATE
    public PlotDto updatePlot(PlotDto plot);

    //DELETE
    public void deletePlot(PlotDto plot);

}
*/
