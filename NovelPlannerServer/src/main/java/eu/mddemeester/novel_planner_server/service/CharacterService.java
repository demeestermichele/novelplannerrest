package eu.mddemeester.novel_planner_server.service;

import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;

import java.util.List;

public interface CharacterService {

    //READ
    public Character getCharacterById(Long id);
//    public List<Character> getAll();


}
