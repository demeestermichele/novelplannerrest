package eu.mddemeester.novel_planner_server.service;

import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import eu.mddemeester.novel_planner_server.mapper.CharacterMapper;
import eu.mddemeester.novel_planner_server.repository.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CharacterServiceImpl implements CharacterService {

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private CharacterMapper characterMapper;


    @Autowired
    public CharacterServiceImpl(CharacterRepository characterRepository){
        this.characterRepository = characterRepository;
    }

    @Override
    public Character getCharacterById(Long id) {
        return characterMapper.toModel(characterRepository.getCharacterById(id));
    }

}
