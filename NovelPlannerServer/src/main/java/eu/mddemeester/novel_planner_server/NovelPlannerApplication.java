package eu.mddemeester.novel_planner_server;

/**
 * @Author: Michele de Meester
 */


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class NovelPlannerApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(NovelPlannerApplication.class, args);
   /*     GenerateData generateData = new GenerateData(ctx);
        generateData.dataInsert();

*/

    }
}
