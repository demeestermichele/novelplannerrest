package eu.mddemeester.novel_planner_server.dto;

import eu.mddemeester.novel_planner_server.domain.Sex;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class CharacterDto implements Serializable {

    //VARIABLES
    private Long id;
    private String firstName;
    private String lastName;
    private Sex sex;
    private Long age;
//    private Long birthday;
    private Boolean alive;
//    private LocalDateTime death;
//    private Race race;
    private CharacterDto mother;
    private CharacterDto father;
    private String image;



    //FOR TESTING (why does lombok not work here?)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
