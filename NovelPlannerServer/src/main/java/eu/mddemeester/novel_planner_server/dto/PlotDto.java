/*
package eu.mddemeester.novel_planner_server.dto;

import java.util.List;

public class PlotDto {

    private Long id;
    private String name;
    private String description;
    private List<CharacterDto> characterList;
    private List<ChapterDto> chapterList;

    public PlotDto() {
    }

    public PlotDto(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CharacterDto> getCharacterList() {
        return characterList;
    }

    public void setCharacterList(List<CharacterDto> characterList) {
        this.characterList = characterList;
    }

    public List<ChapterDto> getChapterList() {
        return chapterList;
    }

    public void setChapterList(List<ChapterDto> chapterList) {
        this.chapterList = chapterList;
    }
}
*/
