package eu.mddemeester.novel_planner_server.mapper;

import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import javax.validation.ValidationException;

/**
 * @Author Michele de Meester
 */

@Mapper(componentModel = "spring")
public interface CharacterMapper { //TEST good

    CharacterMapper INSTANCE = Mappers.getMapper(CharacterMapper.class);

    @Mappings({
    @Mapping(source = "characterDto.id", target = "id"),
    @Mapping(source = "characterDto.firstName", target = "firstName"),
    @Mapping(source = "characterDto.lastName", target = "lastName"),
    @Mapping(source = "characterDto.sex", target = "sex"),
    @Mapping(source = "characterDto.age", target = "age"),
//    @Mapping(source = "characterDto.birthday", target = "birthday"),
    @Mapping(source = "characterDto.alive", target = "alive"),
//    @Mapping(source = "characterDto.death", target = "death")
    })
    Character toModel(CharacterDto characterDto);

    @InheritConfiguration
    void updateModel(CharacterDto characterDto, @MappingTarget Character character);


    @Mappings({
    @Mapping(source = "character.id", target = "id"),
    @Mapping(source = "character.firstName", target = "firstName"),
    @Mapping(source = "character.lastName", target = "lastName"),
    @Mapping(source = "character.sex", target = "sex"),
    @Mapping(source = "character.age", target = "age"),
//    @Mapping(source = "character.birthday", target = "birthday"),
    @Mapping(source = "character.alive", target = "alive"),
//    @Mapping(source = "character.death", target = "death")
    })
    CharacterDto toDto(Character character) throws ValidationException;



}
