package eu.mddemeester.novel_planner_server.mapper;

import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import org.mapstruct.Mapper;

@Mapper
public interface EntityToDtoMapper {

    Character characterToDto(CharacterDto characterDto);
    CharacterDto dtoToCharacter(Character character);
}
