package eu.mddemeester.novel_planner_server.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "CHARACTERS")
public class Character implements Serializable {

    //VARIABLES
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "SEX") //enumeration this might chance because shape-shifters are a thing
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(name = "AGE")
    private Long age;
/*

    @Column(name = "BIRTHDAY") //date of birth
    private LocalDateTime birthday;
*/

    @Column(name = "ALIVE")
    private Boolean alive;
/*

    @Column(name = "DEATH") //date of passing
    private LocalDateTime death;
*/

 /*   @OneToOne(cascade = CascadeType.ALL) //what about double race?
    @JoinColumn(name = "CHARACTER_RACE") //, referencedColumnName = "CHARACTER_ID"
    private Race race;

*/
    @OneToOne(cascade = CascadeType.ALL) //one character has one mother but mother can have more children?
    @JoinColumn(name = "CHARACTER_MOTHER") //, referencedColumnName = "CHARACTER_ID"
    private Character mother;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CHARACTER_FATHER") //, referencedColumnName = "CHARACTER_ID"
    private Character father;

/*    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CHARACTER_PERSONALITIES", joinColumns = @JoinColumn(name = "CHARACTER_ID"),
            inverseJoinColumns = @JoinColumn(name = "PERSONALITY_ID"))
    private List<Personality> personalityList = new ArrayList<>();*/

    @Column(name = "DESCRIPTION") //should this have its own class?
    private String description;

    @Column(name = "IMAGE")
    private String image;

 /*   @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CHARACTER_CHAPTERS", joinColumns = @JoinColumn(name = "CHARACTER_ID"),
            inverseJoinColumns = @JoinColumn(name = "CHAPTER_ID"))
    private List<Chapter> chapterList = new ArrayList<>();
*/
/*
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CHARACTER_PLOTS", joinColumns = @JoinColumn(name = "CHARACTER_ID"),
            inverseJoinColumns = @JoinColumn(name = "PLOT_ID"))
    private List<Plot> plotList = new ArrayList<>();

*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
