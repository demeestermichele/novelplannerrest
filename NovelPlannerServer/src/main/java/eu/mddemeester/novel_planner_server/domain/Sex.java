package eu.mddemeester.novel_planner_server.domain;

public enum Sex {
    MALE, FEMALE, THERIAN
}
