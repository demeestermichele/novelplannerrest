/*
package eu.mddemeester.novel_planner_server.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "RACE")

public class Race implements Serializable {
    //VARIABLES
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "PLANET")
    private String planet;

    @Column(name = "DESCRIPTION")
    private String description;


    //CONSTRUCTORS

    public Race() {
    }

    public Race(Long id, String planet, String description) {
        this.id = id;
        this.planet = planet;
        this.description = description;
    }


    //GETTERS AND SETTERS


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlanet() {
        return planet;
    }

    public void setPlanet(String planet) {
        this.planet = planet;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Race{" +
                "id=" + id +
                ", planet='" + planet + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
*/
