package eu.mddemeester.novel_planner_server.domain;

public enum Type {
    PROTAGONIST, ANTAGONIST, MAIN, SECONDARY, SIDE
}
