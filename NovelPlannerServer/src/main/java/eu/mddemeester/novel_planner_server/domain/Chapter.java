/*
package eu.mddemeester.novel_planner_server.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Entity
@Table(name = "CHAPTER")
public class Chapter implements Serializable {

    //VARIABLES
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CHAPTER_NUMBER")
    private Long number;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;


    @ManyToMany(mappedBy = "chapterList", cascade = CascadeType.ALL)
    private List<Character> characterList =new ArrayList<>();


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CHAPTER_PLOT", joinColumns = @JoinColumn(name = "CHAPTER_ID"),
    inverseJoinColumns = @JoinColumn(name = "PLOT_ID"))
    private List<Plot> plotList =new ArrayList<>();

    //CONSTRUCTOR
    public Chapter() {
    }

    public Chapter(Long number, String name, String description) {
        this.number = number;
        this.name = name;
        this.description = description;
    }

    //GETTERS AND SETTERS
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Character> getCharacterList() {
        return characterList;
    }

    public void setCharacterList(List<Character> characterList) {
        this.characterList = characterList;
    }

    public List<Plot> getPlotList() {
        return plotList;
    }

    public void setPlotList(List<Plot> plotList) {
        this.plotList = plotList;
    }

    @Override
    public String toString() {
        return "Chapter{" +
                "id=" + id +
                ", number=" + number +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", characterList=" + characterList +
                ", plotList=" + plotList +
                '}';
    }
}
*/
