/*
package eu.mddemeester.novel_planner_server.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PLOT")
public class Plot  implements Serializable {

    //VARIABLES
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany(mappedBy = "plotList",cascade = CascadeType.ALL)
    private List<Character> characterList = new ArrayList<>();

    @ManyToMany( mappedBy = "plotList" ,cascade = CascadeType.ALL)
    private List<Chapter> chapterList = new ArrayList<>();

    //CONSTRUCTORS
    public Plot() {
    }

    public Plot(Long id, String plot, String description) {
        this.id = id;
        this.name = plot;
        this.description = description;
    }


    //GETTERS AND SETTERS
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlot() {
        return name;
    }

    public void setPlot(String plot) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Character> getCharacterList() {
        return characterList;
    }

    public void setCharacterList(List<Character> characterList) {
        this.characterList = characterList;
    }

    public List<Chapter> getChapterList() {
        return chapterList;
    }

    public void setChapterList(List<Chapter> chapterList) {
        this.chapterList = chapterList;
    }

    @Override
    public String toString() {
        return "Plot{" +
                "id=" + id +
                ", plot='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
*/
