/*
package eu.mddemeester.novel_planner_server.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= "PERSONALITY")
public class Personality  implements Serializable {

    //THIS MIGHT BE AN ENUM
    //VARIABLES
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TYPE")
    private String type;

    //CONSTRUCTORS
    public Personality() {
    }

    public Personality(Long id, String type) {
        this.id = id;
        this.type = type;
    }

    //GETTERS AND SETTERS
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Personality{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
*/
