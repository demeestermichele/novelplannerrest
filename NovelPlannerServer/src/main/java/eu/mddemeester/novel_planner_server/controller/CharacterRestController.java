package eu.mddemeester.novel_planner_server.controller;

import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.service.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/characters")
public class CharacterRestController {

    @Autowired
    private final CharacterService characterService;

    @Autowired
    public CharacterRestController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Character> getCharacterById(@PathVariable Long id){
        Character character = characterService.getCharacterById(id);
        return ResponseEntity.ok(character);

    }
/*
    @GetMapping
    public ResponseEntity<List<Character>> getCharacters(){
        List<Character> characters = characterService.getAll();
        return ResponseEntity.ok(characters);
    }*/


}
