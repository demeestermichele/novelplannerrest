package eu.mddemeester.novel_planner_server.mapper;

import eu.mddemeester.novel_planner_server.domain.Character;
import eu.mddemeester.novel_planner_server.dto.CharacterDto;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.Assert.assertEquals;


class CharacterMapperTest {

    private CharacterMapper mapper = Mappers.getMapper(CharacterMapper.class);

@Test
public void CharacterToDto(){
    Character character = new Character();
    character.setId(1L);
    character.setLastName("BoTest");
    character.setLastName("Stingray");
    CharacterDto dto = mapper.toDto(character);

    assertEquals(character.getId(), dto.getId());
    assertEquals(character.getFirstName(), dto.getFirstName());
    assertEquals(character.getLastName(), dto.getLastName());

}

    @Test
    public void DtoToCharacter() {
    CharacterDto characterDto = new CharacterDto();
    characterDto.setId(2L);
    characterDto.setFirstName("BillyJoe");
    characterDto.setLastName("Manslow");
    Character character = mapper.toModel(characterDto);

    assertEquals(characterDto.getId(), character.getId());
    assertEquals(characterDto.getFirstName(), character.getFirstName());
    assertEquals(characterDto.getLastName(), character.getLastName());

    }
}